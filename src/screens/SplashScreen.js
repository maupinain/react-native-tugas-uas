import React, { useEffect, } from 'react';
import { View, Text, Image } from 'react-native';

const Splash = ({ navigation }) => {

    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Home')
        }, 3000)
    })

    return (
        <View style={{alignSelf:'center',justifyContent:'center',height:650,}}>
            <Image source={require('../../images/book-removebg-preview.png')}
                style={{ width: 200, height: 200, alignSelf: 'center' }}
            ></Image>
             <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'row', backgroundColor: '#F5F5F5', borderRadius: 2, margin: 2, padding: 1 }}>
                  <Text style={{ fontSize: 30, fontWeight: "bold", fontStyle: 'italic', color: '#BB7E09' }}>My</Text>
                  <Text style={{ fontSize: 30, fontWeight: "bold", fontStyle: 'italic', color: '#19056A', }}>Books</Text>
                </View>

              </View>
        </View>
    );

};
export default Splash;