import { ScrollView, SafeAreaView, View, Text, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import styles from './HomeStyle'
import AccountScreen from './AccountScreen'

export default function HomeScreen({ navigation }) {
  return (
    <ScrollView>
      <SafeAreaView>
        <View style={styles.container2}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
            <View >
              <Image
                source={require('../../images/book-removebg-preview.png')}
                style={{ width: 50, height: 50, }}
              />
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'row', backgroundColor: '#F5F5F5', borderRadius: 2, margin: 2, padding: 1 }}>
                  <Text style={{ fontSize: 10, fontWeight: "bold", fontStyle: 'italic', color: '#BB7E09' }}>My</Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold", fontStyle: 'italic', color: '#19056A', }}>Books</Text>
                </View>

              </View>
            </View>
            <TouchableOpacity >
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=50' }}
                style={{ width: 40, height: 40, borderRadius: 20, margin: 5, borderWidth: 2, borderColor: '#BB7E09' }}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{}}>
            <Image
              source={{ uri: 'https://picsum.photos/200/300?random=34' }}
              style={{ width: 180, height: 90, borderRadius: 5, marginTop: -50, marginLeft: 'auto', marginRight: 'auto' }}
            />
            <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#000000' }}>Cinta bertepuk sebelah tangan</Text>
            <Text style={{ fontSize: 9, alignSelf: 'center' }}>Hati Rania diobati Bhanu - dokter sekaligus duda tampan</Text>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Text style={{ marginTop: 15, color: '#000000' }}>Selamat Datang, Mauphii</Text>
              <TouchableOpacity>
              <Text style={{ marginTop: 25, fontSize: 9, color: '#ffffff',marginRight:1,backgroundColor:'#BB7E09',padding:1,borderRadius:2,}}>Preferensi Konten</Text>
              </TouchableOpacity>
            </View>
          </View>

        </View>


        <View style={styles.container}>
          <View >
            <Text style={{ color: '#BB7E09', margin: 0, }}>Rekomendasi Cerita Untuk Kamu:</Text>
          </View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{ flexDirection: 'row' }}>

              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=2' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=3' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=4' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=5' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=6' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fantasi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=7' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=8' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=9' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=10' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>

            </View>

          </ScrollView>
        </View>

        <View style={styles.container}>
          <View >
            <Text style={{ color: '#BB7E09' }}>Cerita Ter-Populer:</Text>
          </View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=11' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=12' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=14' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=14' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=15' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fantasi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=16' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=17' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=18' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=19' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>

            </View>

          </ScrollView>
        </View>

        <View style={styles.container}>
          <View >
            <Text style={{ color: '#BB7E09' }}>Temukan cerita Faforit Kamu:</Text>
          </View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=20' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=21' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=22' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=23' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=24' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fantasi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=25' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=26' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=27' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=28' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>

            </View>

          </ScrollView>
        </View>

        <View style={styles.container}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=29' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=30' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=4' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=5' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=6' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fantasi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=7' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=8' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=9' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=10' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>

            </View>

          </ScrollView>
        </View>

        <View style={styles.container}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=2' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=3' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=4' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=5' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=6' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fantasi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=7' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Horor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=8' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Fiksi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=9' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Humor</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Image
                    source={{ uri: 'https://picsum.photos/200/300?random=10' }}
                    style={{ width: 60, height: 90, margin: 10 }}
                  />
                  <Text style={{ alignSelf: 'center' }}>Dewasa</Text>
                </View>
              </TouchableOpacity>

            </View>

          </ScrollView>
        </View>



      </SafeAreaView >
    </ScrollView>

  )
}