import { ScrollView, SafeAreaView, View, Text, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import css from './AccountStyle'

export default function AccountScreen() {
  return (
    <ScrollView>
      <SafeAreaView>


        <View style={css.container}>
          <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
            <View style={{}}>
              <View style={{ flexDirection: 'row', backgroundColor: '#F5F5F5', borderRadius: 2, marginTop: 1, margin: 1, padding: 1, }}>
                <Text style={{ fontSize: 10, fontWeight: "bold", fontStyle: 'italic', color: '#BB7E09' }}>My</Text>
                <Text style={{ fontSize: 10, fontWeight: "bold", fontStyle: 'italic', color: '#19056A', }}>Books</Text>
              </View>

            </View>

            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=50' }}
                style={{ width: 90, height: 90, borderRadius: 45, margin: 5, borderWidth: 2, borderColor: '#BB7E09' }}
              />
            </View>
            <TouchableOpacity>
              <View>
                <Image
                  source={require('../../images/settings-removebg-preview.png')}
                  style={{ width: 25, height: 25, }}
                />
              </View>
            </TouchableOpacity>
          </View>

        </View>
        <View style={css.container2}>
          <Text style={{ color: '#BB7E09', alignSelf: 'center' }}>Mauphii</Text>
          <Text style={{ alignSelf: 'center', fontSize: 9 }}>maupinain@gmail.com</Text>
        </View>
        <View style={css.container2}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 3 }}>
            <Text style={{ color: '#000000', fontWeight: 'bold', margin: 1 }}>Daftar Bacaan :</Text>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity>
                <Text style={{ color: 'red', fontWeight: 'normal', fontSize: 10, backgroundColor: '#BB7E09', margin: 1, padding: 1, borderRadius: 2, marginTop: 10 }}>+</Text>
              </TouchableOpacity>
              <Text style={{ color: '#ffffff', fontWeight: 'normal', fontSize: 8, backgroundColor: '#BB7E09', margin: 1, padding: 1, borderRadius: 2, marginTop: 10 }}>Tambah Daftar Bacaan</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around', flexWrap: 'wrap' }}>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=82' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=81' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=80' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=79' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=61' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=78' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=77' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=76' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=75' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=74' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=73' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=72' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>

                <Image
                  source={{ uri: 'https://picsum.photos/200/300?random=71' }}
                  style={{ width: 60, height: 90, margin: 5, }}
                />
              </TouchableOpacity>
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=60' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=59' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=57' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=56' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=55' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=54' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=53' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=52' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=51' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=61' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=60' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=59' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=57' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=56' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=55' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=54' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=53' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=52' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
            <View>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=51' }}
                style={{ width: 60, height: 90, margin: 5, }}
              />
            </View>
          </View>
        </View>

      </SafeAreaView>
    </ScrollView>
  )
}