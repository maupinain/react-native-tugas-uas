import { ScrollView, SafeAreaView, View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import React from 'react'
import perpus from './LibraryStyle'

export default function LibraryScreen() {
  return (

    <SafeAreaView>
      <View style={perpus.container}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flexDirection: 'row', backgroundColor: '#F5F5F5', borderRadius: 2, margin: 2, padding: 1 }}>
            <Text style={{ fontSize: 10, fontWeight: "bold", fontStyle: 'italic', color: '#BB7E09' }}>My</Text>
            <Text style={{ fontSize: 10, fontWeight: "bold", fontStyle: 'italic', color: '#19056A', }}>Books</Text>
          </View>

        </View>
        <View>
          <Text style={{ color: '#ffffff', alignSelf: 'center' }}>My Library</Text>
        </View>

      </View>

      <View style={perpus.container2}>
        <View style={{flexDirection:'row',justifyContent:'space-between',margin:10}}>

          <View>
            <TouchableOpacity>
              <Text style={{ fontSize: 10 }}>Bacaan Saat Ini</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity>
              <Text style={{ fontSize: 10 }}>Arsip</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity>
              <Text style={{ fontSize: 10 }}>Daftar Bacaan</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

    </SafeAreaView>

  )
}