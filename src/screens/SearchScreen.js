import { ScrollView, SafeAreaView, View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import React from 'react'
import stail from './SearchStyel'

export default function SearchScreen() {
  return (
    <SafeAreaView>
      <View style={stail.container2}>
        <TouchableOpacity>
<View style={stail.container}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <TextInput>
              <Text style={{ marginRight: 10, }}>search</Text>
            </TextInput>
            <Image
              source={require('../../images/search.png')}
              style={{ width: 45, height: 45, }}
            />
          </View>
          </View>
        </TouchableOpacity>
      </View>
      <View style={stail.container3}>
        <Text style={{ fontWeight: 'bold', color: '#000000',margin:5 }}>Jelajahi Katagori </Text>
      </View>

      <ScrollView>
        <View style={stail.container2}>

          <View style={{ flexDirection: 'row', justifyContent: 'center', flexWrap: 'wrap' }}>

            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Acak </Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=70' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Fantasi</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=71' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Klasik</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=72' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Roman</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=73' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Vampir</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=74' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Fiksi</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=75' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Dewasa</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=76' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Laga</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=77' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Humor</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=78' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Horor</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=79' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Puisi</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=81' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Cerpen</Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=83' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: '#BB7E09', margin: 15, padding: 4, borderRadius: 4 }}>

              <Text style={{ color: '#ffffff', backgroundColor: '#19056A', fontWeight: 'bold', margin: 1, padding: 5, borderRadius: 4 }}>Fantasi </Text>
              <Image
                source={{ uri: 'https://picsum.photos/200/300?random=84' }}
                style={{ width: 50, height: 50, margin: 1, borderRadius: 4 }}
              />
            </View>
          </View>

        </View>
      </ScrollView>
    </SafeAreaView>

  )
}